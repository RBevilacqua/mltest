//
//  UIImageExtension.swift
//  MLTest
//
//  Created by Robert Bevilacqua on 3/15/18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    class func imageFromColor(color: UIColor) -> UIImage {
        return imageFromColor(color: color, size: CGSize(width: 10, height: 10))
        
    }
    
    class func imageFromColor(color: UIColor, size: CGSize) -> UIImage {
        let imageView = UIView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        imageView.backgroundColor = color
        return imageView.convertToImage()
    }
    
}
