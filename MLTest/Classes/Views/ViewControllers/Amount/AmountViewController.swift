//
//  AmountViewController.swift
//  MLTest
//
//  Created by Robert Bevilacqua on 3/14/18.
//  Copyright © 2018 Robert Bevilacqua. All rights reserved.
//

import UIKit

class AmountViewController: BaseViewController {

    @IBOutlet weak var amountTextField: UITextField!
    
    var amount: Float?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureScreen()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureScreen() -> Void {
        self.amountTextField.underlined()
    }
    
    @IBAction func payAction(_ sender: UIButton) {
        
        self.amountTextField.resignFirstResponder()
        if let amount = Float(amountTextField.text!) {
            self.amount = amount
            let vc = PaymentMethodsViewController(nibName: "PaymentMethodsViewController", bundle: nil)
            vc.amount = self.amount
            vc.delegate = self
            navigationController?.pushViewController(vc, animated: true)
        } else {
            self.amountTextField.shake()
        }
        
        
        
    }

}

extension AmountViewController: PaymentDelegate {
    
    func retrievePayment(retrieve: (issuer: IssuerCard, payer: PayerCost, payment: PaymentMethods)) {
        guard let amountViewController = navigationController?.viewControllers.first(where: { $0 is AmountViewController }) else {
            // Podrias hacer un fallback a rootViewController en caso de que no encuentre el AmountViewControler en el stack de navigation
            
            navigationController?.popToRootViewController(animated: true)
            return
        }
        navigationController?.popToViewController(amountViewController, animated: true)
        
        DispatchQueue.main.async {
            let alert = MLAlertView()
            alert.setData(payment: retrieve.payment, issuer: retrieve.issuer, payer: retrieve.payer, amount: self.amount!)
            alert.show()
            
        }
        
    }
    
    
    
    
}
